/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 92.5, "KoPercent": 7.5};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.415625, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [1.0, 500, 1500, "GET Buyer Order "], "isController": false}, {"data": [0.65, 500, 1500, "GET Seller Product id"], "isController": false}, {"data": [0.4, 500, 1500, "GET Buyer Product id"], "isController": false}, {"data": [0.65, 500, 1500, "DELETED Seller Product id"], "isController": false}, {"data": [0.95, 500, 1500, "GET Buyer Order id"], "isController": false}, {"data": [0.45, 500, 1500, "POST Register"], "isController": false}, {"data": [1.0, 500, 1500, "POST Login"], "isController": false}, {"data": [0.75, 500, 1500, "GET Seller Product"], "isController": false}, {"data": [0.0, 500, 1500, "POST Product"], "isController": false}, {"data": [0.0, 500, 1500, "GET Buyer Product"], "isController": false}, {"data": [0.4, 500, 1500, "POST Buyer Order"], "isController": false}, {"data": [0.4, 500, 1500, "PUT Buyer Order id"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 160, 12, 7.5, 5891.7249999999985, 252, 82295, 1095.5, 3631.1000000000004, 62040.84999999998, 81823.46999999999, 1.4627635259914795, 540.0671321321149, 304.1294642857143], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["GET Buyer Order ", 10, 0, 0.0, 279.90000000000003, 262, 336, 272.5, 332.40000000000003, 336.0, 336.0, 0.3480076561684357, 0.2209032973725422, 0.11792837567426484], "isController": false}, {"data": ["GET Seller Product id", 10, 0, 0.0, 777.2, 257, 1759, 580.5, 1753.6, 1759.0, 1759.0, 1.2717792191275594, 0.8780741288312348, 0.4421419941498156], "isController": false}, {"data": ["GET Buyer Product id", 10, 0, 0.0, 1438.4, 994, 3560, 1081.5, 3418.3, 3560.0, 3560.0, 0.32778287662252525, 0.27560650075390064, 0.11363566523534811], "isController": false}, {"data": ["DELETED Seller Product id", 10, 0, 0.0, 935.3000000000001, 257, 2847, 670.5, 2726.3, 2847.0, 2847.0, 1.1604966925844262, 0.3649218115353371, 0.4283864744110479], "isController": false}, {"data": ["GET Buyer Order id", 10, 0, 0.0, 337.2, 253, 887, 270.0, 829.9000000000002, 887.0, 887.0, 0.3475359699728922, 0.22033237471328282, 0.11878670848682839], "isController": false}, {"data": ["POST Register", 10, 0, 0.0, 1302.5, 1078, 2636, 1144.5, 2509.6000000000004, 2636.0, 2636.0, 1.7873100983020553, 1.0105981903485255, 2.702259271671135], "isController": false}, {"data": ["POST Login", 10, 0, 0.0, 334.3, 311, 380, 330.0, 378.6, 380.0, 380.0, 2.3293733985557887, 1.1578623631493128, 0.680158834148614], "isController": false}, {"data": ["GET Seller Product", 10, 0, 0.0, 617.7, 277, 1735, 297.5, 1719.4, 1735.0, 1735.0, 1.3785497656465397, 3.271363213399504, 0.4711840019299697], "isController": false}, {"data": ["POST Product", 50, 0, 0.0, 3081.52, 2470, 5384, 2943.0, 3735.0, 4076.8499999999976, 5384.0, 2.310856403383094, 1.50747273189444, 1535.1275448306142], "isController": false}, {"data": ["GET Buyer Product", 10, 0, 0.0, 72230.3, 58653, 82295, 75675.5, 82217.7, 82295.0, 82295.0, 0.11193570413154684, 660.0334705245307, 0.0430690111599897], "isController": false}, {"data": ["POST Buyer Order", 10, 6, 60.0, 278.6000000000001, 253, 336, 275.5, 330.5, 336.0, 336.0, 0.34867503486750345, 0.16459913267085077, 0.14301124476987448], "isController": false}, {"data": ["PUT Buyer Order id", 10, 6, 60.0, 328.59999999999997, 252, 818, 276.0, 766.8000000000002, 818.0, 818.0, 0.3546979746745646, 0.19065016138757845, 0.13786112687546553], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["400/Bad Request", 6, 50.0, 3.75], "isController": false}, {"data": ["404/Not Found", 6, 50.0, 3.75], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 160, 12, "400/Bad Request", 6, "404/Not Found", 6, "", "", "", "", "", ""], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["POST Buyer Order", 10, 6, "400/Bad Request", 6, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["PUT Buyer Order id", 10, 6, "404/Not Found", 6, "", "", "", "", "", "", "", ""], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
