/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.6071428571428571, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [1.0, 500, 1500, "GET Buyer Order "], "isController": false}, {"data": [1.0, 500, 1500, "GET Seller Product id"], "isController": false}, {"data": [0.75, 500, 1500, "GET Buyer Product id"], "isController": false}, {"data": [1.0, 500, 1500, "DELETED Seller Product id"], "isController": false}, {"data": [1.0, 500, 1500, "GET Buyer Order id"], "isController": false}, {"data": [0.0, 500, 1500, "POST Register"], "isController": false}, {"data": [0.75, 500, 1500, "POST Login"], "isController": false}, {"data": [1.0, 500, 1500, "GET Seller Product"], "isController": false}, {"data": [0.0, 500, 1500, "POST Product"], "isController": false}, {"data": [0.0, 500, 1500, "GET Buyer Product"], "isController": false}, {"data": [1.0, 500, 1500, "POST Buyer Order"], "isController": false}, {"data": [1.0, 500, 1500, "PUT Buyer Order id"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 28, 0, 0.0, 5937.928571428571, 292, 32765, 437.0, 21228.500000000007, 29487.19999999998, 32765.0, 0.2859389520337408, 110.10043845036407, 40.80550893623051], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["GET Buyer Order ", 2, 0, 0.0, 318.5, 292, 345, 318.5, 345.0, 345.0, 345.0, 0.06788635823631241, 0.08061505040562099, 0.022606687654865756], "isController": false}, {"data": ["GET Seller Product id", 2, 0, 0.0, 409.5, 341, 478, 409.5, 478.0, 478.0, 478.0, 0.05601770159370361, 0.038676284205809035, 0.019146675349410412], "isController": false}, {"data": ["GET Buyer Product id", 2, 0, 0.0, 807.5, 319, 1296, 807.5, 1296.0, 1296.0, 1296.0, 0.06539153179663233, 0.05498252820009809, 0.022286762301781917], "isController": false}, {"data": ["DELETED Seller Product id", 2, 0, 0.0, 365.5, 317, 414, 365.5, 414.0, 414.0, 414.0, 0.05590183637532493, 0.017578507141459598, 0.02030808899572351], "isController": false}, {"data": ["GET Buyer Order id", 2, 0, 0.0, 339.0, 329, 349, 339.0, 349.0, 349.0, 349.0, 0.06787483879725785, 0.0804688030272178, 0.023000555725242653], "isController": false}, {"data": ["POST Register", 2, 0, 0.0, 2833.0, 1881, 3785, 2833.0, 3785.0, 3785.0, 3785.0, 0.4666355576294914, 0.2615711035930938, 0.702003980984601], "isController": false}, {"data": ["POST Login", 2, 0, 0.0, 450.0, 382, 518, 450.0, 518.0, 518.0, 518.0, 0.6837606837606838, 0.3325320512820513, 0.1963141025641026], "isController": false}, {"data": ["GET Seller Product", 2, 0, 0.0, 327.0, 324, 330, 327.0, 330.0, 330.0, 330.0, 0.05625087891998312, 0.08624402334411475, 0.01889677963718183], "isController": false}, {"data": ["POST Product", 6, 0, 0.0, 20886.166666666668, 13630, 32765, 19641.5, 32765.0, 32765.0, 32765.0, 0.07357629862167067, 0.04799703855398048, 48.87636154473439], "isController": false}, {"data": ["GET Buyer Product", 2, 0, 0.0, 13884.5, 11286, 16483, 13884.5, 16483.0, 16483.0, 16483.0, 0.04278166377890436, 230.1904185116259, 0.01621023979122548], "isController": false}, {"data": ["POST Buyer Order", 2, 0, 0.0, 399.5, 339, 460, 399.5, 460.0, 460.0, 460.0, 0.0675014344054811, 0.04541844561071923, 0.027290618988153496], "isController": false}, {"data": ["PUT Buyer Order id", 2, 0, 0.0, 338.5, 293, 384, 338.5, 384.0, 384.0, 384.0, 0.06796017533725238, 0.0453952733698053, 0.02621510669747528], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 28, 0, "", "", "", "", "", "", "", "", "", ""], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
